﻿using MVCPS.Models;
using System.Web.Mvc;

namespace MVCPS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            PsInterpreter interpreter = new PsInterpreter();

            return View(interpreter);
        }



    }
}