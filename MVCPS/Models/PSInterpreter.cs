﻿using Microsoft.Ajax.Utilities;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Management.Automation;
using System.Net;
using System.Text;

namespace MVCPS.Models
{
    public class PsInterpreter
    {
        [Display(Name = "PS Command")]
        [Required(ErrorMessage = "The command cannot be empty")]
        [DataType(DataType.Text)]
        public string Command { get; set; }


        [Display(Name = "Result")]
        [DataType(DataType.MultilineText)]
        public string Result { get; set; }


        public Collection<PSObject> Interpreter()
        {
            if (!Command.IsNullOrWhiteSpace())
            {
                WebClient client = new WebClient();
                var pageData = client.DownloadData("http://localhost:58848/api/PsApi");
                string pageHtml = Encoding.ASCII.GetString(pageData);

            }
            return new Collection<PSObject>();
        }

        public string InterpreterString()
        {
            if (!Command.IsNullOrWhiteSpace())
            {
                using (PowerShell shell = PowerShell.Create())
                {
                    shell.Commands.AddScript(Command + " | out-string");
                    /*IAsyncResult result = shell.BeginInvoke();

                    while (result.IsCompleted == false)
                    {
                        Console.WriteLine("Waiting for pipeline to finish...");
                        Thread.Sleep(1000);
                        // might want to place a timeout here...
                    }

                 
                    StringBuilder sb = new StringBuilder();
                    
                     foreach (var r in Task.FromResult(result))
                    // {
                    //     sb.AppendLine(r);
                    // }

    */

                    var result = shell.Invoke();
                    StringBuilder sb = new StringBuilder();
                    if (result.Count > 0)
                    {
                        foreach (PSObject r in result)
                        {
                            sb.AppendLine($"{r}");
                        }
                    }
                    return sb.ToString();

                }
            }
            return string.Empty;
        }


    }
}