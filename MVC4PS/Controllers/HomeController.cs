﻿using MVC4PS.Models;
using System.Web.Mvc;


namespace MVC4PS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(PsInterpreter inter)
        {
            inter.Result = inter.InterpreterString();
            return PartialView("_index", inter);


        }
    }
}